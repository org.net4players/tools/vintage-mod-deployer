import os
import re
import sys
import requests

VERSION = '0.1.2'

class VintageUpload:
    def __init__(self, modId):

        self.modId = modId
        self.session = requests.Session()
        self.uploaded_file_id = None

        self.default_header = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.53 Safari/537.36 Edg/103.0.1264.37'
        }


    def setText(self, text):
        self.mod_text = text
        return self

    def setTagIds(self, ids):
        self.mod_tagids = ids
        return self

    def login(self):
        print("Login to VintageModDB via Username: " + os.getenv('VINTAGE_EMAIL'))

        response = self.session.post('https://account.vintagestory.at/attemptlogin',
                          data={'loginredir': 'mods', 'email': os.getenv('VINTAGE_EMAIL'),
                                'password': os.getenv('VINTAGE_PASSWORD'), }, headers=self.default_header)
        if 'Invalid email or password' in response.text:
            print("Invalid email or password")
            sys.exit(1)

    def deletePendingTempFile(self):
        print("Deleting pending temporary files... paranoid mode ;)")

        pending_file_id = self._get_pending_file_id(self.modId)
        if not pending_file_id:
            print("No temporary files detected")
            return True

        print(f"Detected File-ID {pending_file_id}")
        csrf_token = self._get_csrf_token(self.modId)
        if not csrf_token:
            print("Can not detect CSRF Token.. :(")
            return False

        print(f"Detected CSRF Token {csrf_token}")

        payload = {
            'fileid':  pending_file_id,
            'at': csrf_token
        }

        response = self.session.post(
            'https://mods.vintagestory.at/edit-deletefile',
            data=payload,
            headers=self.default_header
        )
        print("Temporary file removed")


    def uploadFile(self, fileName):
        print(f"Upload File {fileName}")

        if not os.path.exists(fileName):
            print(f"File {fileName} doesnt exists")
            sys.exit(1)

        custom_headers = {
            'X-Requested-With': 'XMLHttpRequest',
        }

        files = {'file': open(fileName, 'rb')}

        response = self.session.post(
            'https://mods.vintagestory.at/edit-uploadfile',
            data={
                "upload": 1,
                "assetid": 0,
                "assettypeid": 2
            },
            files=files,
            headers={**self.default_header, **custom_headers},
        )

        if response.status_code != 200:
            sys.exit(1)

        response_data = response.json()

        self.uploaded_file_id = response_data["fileid"]
        return response_data

    def _get_pending_file_id(self, modId):
        content = self.session.get("https://mods.vintagestory.at/edit/release/?modid=" + str(modId))
        regex = "data-fileid=\"([0-9]+)\""
        matches = re.findall(regex, content.text)
        if len(matches) == 0: return False
        return matches[0]

    def _get_csrf_token(self, modId):
        content = self.session.get("https://mods.vintagestory.at/edit/release/?modid=" + str(modId))
        regex = "<input.*name=\"at\".*value=\"(.+)\">"
        matches = re.findall(regex, content.text)
        if len(matches) == 0: return False
        return matches[0]

    def publishMod(self):
        print("Publish")

        if self.uploaded_file_id == None:
            print("First upload a file")
            sys.exit(1)

        csrf_token = self._get_csrf_token(self.modId)
        if not csrf_token:
            print("Can not detect CSRF Token.. :(")
            sys.exit(1)

        print(f"Detected CSRF Token {csrf_token}")

        payload = {
            'at': csrf_token,
            'save': 1,
            'assetid': 0,
            'modid': self.modId,
            'statusid': 0,
            'numsaved': 0,
            'saveandback': 0,
            'tagids[]': self.mod_tagids,
            'text': self.mod_text,
            'fileIds': [self.uploaded_file_id]
        }
        response = self.session.post(
            'https://mods.vintagestory.at/edit/release/?modid=' + str(self.modId),
            data=payload)

        if response.status_code == 200:
            print("Mod Release successfully uploaded")
