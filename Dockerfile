FROM python:latest

RUN mkdir /app

ADD vintage_upload.py /app
ADD run.py /app
ADD requirements.txt /app

RUN pip3 install -r /app/requirements.txt \
 && chmod +x /app/run.py

