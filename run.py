#!/usr/bin/env python3
import argparse
import sys
from pprint import pprint

from vintage_upload import VintageUpload

parser = argparse.ArgumentParser()
parser.add_argument("--modid",  help="Vintage ModID", type=int)
parser.add_argument("--text", help="Vintage Release Description", type=str)
parser.add_argument("--tagids", help="Comma seperated list of compatible game versions", type=str)
parser.add_argument("--file", help="Path to your ZIP mod package", type=str)
args = parser.parse_args()

if __name__ == '__main__':
    upload = VintageUpload(args.modid)
    upload.setText(args.text).setTagIds(args.tagids.split(","))
    upload.login()
    upload.deletePendingTempFile()
    upload.uploadFile(args.file)
    upload.publishMod()
